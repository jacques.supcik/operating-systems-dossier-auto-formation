master = dossier-auto-formation

xelatex = xelatex -synctex=1 -papersize=a5

all:	pdf

pdf:
	[ -d tmp ] || mkdir tmp
	latexmk -outdir=tmp -pdf -pdflatex='$(xelatex)' $(master)
	cp tmp/$(master).pdf .

clean:
	rm tmp/*
	rm $(master).pdf
